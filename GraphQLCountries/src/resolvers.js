import { Search } from "./elasticsearch-client";
const resolvers = {
  Query: {
    localities: async (parent, args, context, info) => {
      return await Search();
    }
  }
};

export default resolvers;