const { Client } = require('@elastic/elasticsearch')
const client = new Client({ node: 'http://localhost:9200' })

export function Search () {
  console.log('start');
  let localities = [];

  return new Promise ((resolve, reject) => {client.search({
  index: 'localities',
  scroll: '10s',
  size: 10000,
  body: {
     query: {
         "match_all": {}
     }
  }
}, function getMoreUntilDone(error, response) {
    console.log(localities.length);
    response.body.hits.hits.forEach(function (hit) {
    localities.push(hit);
  });

  if (response.body.hits.total.value !== localities.length) {
       client.scroll({
      scrollId: response.body._scroll_id,
      scroll: '10s'
    }, getMoreUntilDone);
  } else {
      resolve(localities.map(function(locality){
        return locality._source;
    }));
  }
})});
}