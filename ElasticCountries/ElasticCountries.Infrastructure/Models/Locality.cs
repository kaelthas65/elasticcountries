﻿using System;

namespace ElasticCountries.Infrastructure.Models
{
    public class Locality
    {
        public Locality(
            int geoNameId, 
            string name, 
            string asciiName, 
            string alternateNames, 
            double latitude, 
            double longitude, 
            string featureClass, 
            string featureCode,
            string countryCode,
            string cc2, 
            string admin1Code, 
            string admin2Code, 
            string admin3Code, 
            string admin4Code, 
            int population, 
            int elevation, 
            int dem, 
            string timeZone, 
            DateTime modificationDate)
        {
            GeoNameId = geoNameId;
            Name = name;
            AsciiName = asciiName;
            AlternateNames = alternateNames;
            Latitude = latitude;
            Longitude = longitude;
            FeatureClass = featureClass;
            FeatureCode = featureCode;
            CountryCode = countryCode;
            CC2 = cc2;
            Admin1Code = admin1Code;
            Admin2Code = admin2Code;
            Admin3Code = admin3Code;
            Admin4Code = admin4Code;
            Population = population;
            Elevation = elevation;
            Dem = dem;
            TimeZone = timeZone;
            ModificationDate = modificationDate;
        }

        public int GeoNameId { get; set; }

        public string Name { get; set; }

        public string AsciiName { get; set; }

        public string AlternateNames { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public string FeatureClass { get; set; }

        public string FeatureCode { get; set; }

        public string CountryCode { get; set; }

        public string CC2 { get; set; }

        public string Admin1Code { get; set; }

        public string Admin2Code { get; set; }

        public string Admin3Code { get; set; }

        public string Admin4Code { get; set; }

        public int Population { get; set; }

        public int Elevation { get; set; }

        public int Dem { get; set; }

        public string TimeZone { get; set; }

        public DateTime ModificationDate { get; set; }
    }
}
