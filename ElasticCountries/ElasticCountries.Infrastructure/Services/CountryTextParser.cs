﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using ElasticCountries.Infrastructure.Models;

namespace ElasticCountries.Infrastructure.Services
{
    public class CountryTextParser : ICountryTextParser
    {
        public async Task<List<Locality>> GetLocalities(string path)
        {
            List<Locality> localities = new List<Locality>();

            using (var file = new StreamReader(path))
            {
                string line;

                while ((line = await file.ReadLineAsync()) != null)
                {
                    var terms = line.Split('\t');
                    var featureClass = terms[6];

                    if (featureClass.Equals("A") || featureClass.Equals("P"))
                    {
                        var locality = ToLocalityModel(terms);
                        localities.Add(locality);
                    }
                }
            }

            return localities;
        }

        private Locality ToLocalityModel(string[] terms)
        {
            int geoNameId;
            double latitude;
            double longitude;
            int population;
            int elevation;
            int dem;
            DateTime modificationDate;

            int.TryParse(terms[0], out geoNameId);
            double.TryParse(terms[4], out latitude);
            double.TryParse(terms[5], out longitude);
            int.TryParse(terms[14], out population);
            int.TryParse(terms[15], out elevation);
            int.TryParse(terms[16], out dem);
            DateTime.TryParse(terms[18], out modificationDate);

            var locality = new Locality(
                geoNameId,
                terms[1],
                terms[2],
                terms[3],
                latitude,
                longitude,
                terms[6],
                terms[7],
                terms[8],
                terms[9],
                terms[10],
                terms[11],
                terms[12],
                terms[13],
                population,
                elevation,
                dem,
                terms[17],
                modificationDate
            );

            return locality;
        }
    }
}
