﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ElasticCountries.Infrastructure.Models;

namespace ElasticCountries.Infrastructure.Services
{
    public interface IElasticSearchService
    {
        Task IndexLocalities(List<Locality> localities);
    }
}
