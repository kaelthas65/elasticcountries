﻿using System.Threading.Tasks;

namespace ElasticCountries.Infrastructure.Services
{
    public interface ICountryService
    {
        Task Index();
    }
}
