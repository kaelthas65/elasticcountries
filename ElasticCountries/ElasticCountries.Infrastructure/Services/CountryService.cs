﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ElasticCountries.Infrastructure.Configuration;
using ElasticCountries.Infrastructure.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Options;

namespace ElasticCountries.Infrastructure.Services
{
    public class CountryService : ICountryService
    {
        private readonly CountrySettings countrySettings;
        private readonly ICountryTextParser countryTextParser;
        private readonly IElasticSearchService elasticSearchService;
        private readonly IHostingEnvironment env;

        public CountryService(
            ICountryTextParser countryTextParser,
            IElasticSearchService elasticSearchService,
            IOptions<CountrySettings> options,
            IHostingEnvironment env)
        {
            this.countryTextParser = countryTextParser;
            this.elasticSearchService = elasticSearchService;
            this.env = env;
            countrySettings = options.Value;
        }

        public async Task Index()
        {
            var localities = new List<Locality>();

            foreach (var countryCode in countrySettings.CountryCodes)
            {
                localities.AddRange(await countryTextParser.GetLocalities($"{env.WebRootPath}/Countries/{countryCode}.txt"));
            }

            await elasticSearchService.IndexLocalities(localities);
        }
    }
}
