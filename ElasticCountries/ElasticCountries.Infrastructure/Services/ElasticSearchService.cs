﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ElasticCountries.Infrastructure.Configuration;
using ElasticCountries.Infrastructure.Models;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Nest;

namespace ElasticCountries.Infrastructure.Services
{
    public class ElasticSearchService : IElasticSearchService
    {
        private readonly ElasticClient client;
        private readonly ILogger<ElasticSearchService> logger;

        public ElasticSearchService(
            IOptions<ElasticSearchConfiguration> options,
            ILogger<ElasticSearchService> logger)
        {
            this.logger = logger;
            const string indexName = "localities";
            var elasticUri = options.Value.ElasticSearchUri;
            var node = new Uri(elasticUri);
            var settings = new ConnectionSettings(node);
            settings.DefaultIndex(indexName);
            client = new ElasticClient(settings);
        }

        public async Task IndexLocalities(List<Locality> localities)
        {
            var response = await client.IndexManyAsync(localities);
            logger.LogInformation(response.DebugInformation);
        }
    }
}
