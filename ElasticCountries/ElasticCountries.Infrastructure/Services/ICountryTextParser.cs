﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ElasticCountries.Infrastructure.Models;

namespace ElasticCountries.Infrastructure.Services
{
    public interface ICountryTextParser
    {
        Task<List<Locality>> GetLocalities(string path);
    }
}
