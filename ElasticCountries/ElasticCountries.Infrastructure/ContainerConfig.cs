﻿using ElasticCountries.Infrastructure.Services;
using Microsoft.Extensions.DependencyInjection;

namespace ElasticCountries.Infrastructure
{
    public static class ContainerConfig
    {
        public static IServiceCollection AddInfrastructure(
            this IServiceCollection services)
        {
            services.AddScoped<ICountryTextParser, CountryTextParser>();
            services.AddScoped<ICountryService, CountryService>();
            services.AddSingleton<IElasticSearchService, ElasticSearchService>();

            return services;
        }
    }
}
