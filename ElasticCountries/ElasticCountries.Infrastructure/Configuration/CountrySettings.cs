﻿using System.Collections.Generic;

namespace ElasticCountries.Infrastructure.Configuration
{
    public class CountrySettings
    {
        public List<string> CountryCodes { get; set; }
    }
}
