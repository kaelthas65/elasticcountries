﻿namespace ElasticCountries.Infrastructure.Configuration
{
    public class ElasticSearchConfiguration
    {
        public string ElasticSearchUri { get; set; }
    }
}
