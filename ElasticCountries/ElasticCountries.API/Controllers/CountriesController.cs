﻿using System.Threading.Tasks;
using ElasticCountries.Infrastructure.Services;
using Microsoft.AspNetCore.Mvc;

namespace ElasticCountries.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CountriesController : ControllerBase
    {
        private readonly ICountryService countryService;

        public CountriesController(ICountryService countryService)
        {
            this.countryService = countryService;
        }

        [HttpGet]
        public async Task Get()
        {
            await countryService.Index();
        }
    }
}
